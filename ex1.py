# -*- coding: utf-8 -*-
"""
created on 25/03/2020
by William FLEITH
"""

NUMBER1 = int(input("Give a first integer :"))
NUMBER2 = int(input("Give a second integer :"))


def sum1(number, num):
    """
    function sum
    """
    return number + num


def substract(number, num):
    """
    function substract
    """
    return number - num


def divide(number, num):
    """
    function divide
    """
    return number / num


def multiply(number, num):
    """
    function multiply
    """
    return number * num

#main code
RESULT_1 = sum1(NUMBER1, NUMBER2)
RESULT_2 = substract(NUMBER1, NUMBER2)
RESULT_3 = divide(NUMBER1, NUMBER2)
RESULT_4 = multiply(NUMBER1, NUMBER2)
print("sum =", RESULT_1)
print("substract =", RESULT_2)
print("divide =", RESULT_3)
print("multiply =", RESULT_4)
